﻿
using Microsoft.AspNetCore.Mvc;
using Bank.Api.Bank.Models;


namespace Bank.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BankController : ControllerBase
    {
        [HttpPost]
        [ActionName("Pay")]
        public IActionResult Pay([FromBody] BankData data) {


            if (data.CardNumber.EndsWith('1')) {
                return new OkObjectResult(new BankResult { 
                    BankId =  1,
                    Status = BankStatus.Ok
                });
            }

            if (data.CardNumber.EndsWith('2'))
            {
                return new OkObjectResult(new BankResult
                {
                    BankId = 1,
                    Status = BankStatus.InsufficientMoney
                });
            }


            if (data.CardNumber.EndsWith('3'))
            {
                return new OkObjectResult(new BankResult
                {
                    BankId = 1,
                    Status = BankStatus.CardNotValid
                });
            }

            if (data.CardNumber.EndsWith('4'))
            {
                return new OkObjectResult(new BankResult
                {
                    BankId = 1,
                    Status = BankStatus.CardExpired
                });
            }

            return NoContent();
        }
    }
}