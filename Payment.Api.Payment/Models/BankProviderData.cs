﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Api.Payment.Models
{
    public class BankProviderData
    {
        [Key]
        public string ProviderName { get; set; }
        public string ProviderUrl { get; set; }
    }
}
