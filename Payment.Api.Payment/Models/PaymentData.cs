﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Api.Payment.Models
{
    public enum PaymentStatus
    {
        Ok = 1,
        InsufficientMoney = 2,
        CardNotValid = 3,
        CardExpired = 4,
    }

    public class PaymentData
    {
        [Key]
        public long PaymentId { get; set; }
        public long BankId { get; set; }
        public string CardNumber { get; set; } 
        public DateTime ExpDate { get; set; }
        public DateTime Timestamp { get; set; }
        public decimal Amount { get; set; }
        public PaymentStatus Status { get; set; }
    }
}
