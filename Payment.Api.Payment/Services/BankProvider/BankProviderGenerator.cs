﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Payment.Api.Payment.Services.Payment;
using Payment.Api.Payment.Models;

namespace Payment.Api.Payment.Services.BankProvider
{
    public class BankProviderGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new PaymentContext(
                serviceProvider.GetRequiredService<DbContextOptions<PaymentContext>>()))
            {
                // Look for any data
                if (context.BankProviderItems.Any())
                {
                    return;   // Data was already seeded
                }

                context.BankProviderItems.AddRange(
                    new BankProviderData
                    {
                        ProviderName = "BankProviderVisa",
                        ProviderUrl = "http://localhost:5001/api/Bank/Pay"
                    });

                context.SaveChanges();
            }
        }
    }
}
