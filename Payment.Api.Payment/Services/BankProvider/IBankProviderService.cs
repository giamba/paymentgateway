﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Payment.Api.Payment.Services.BankProvider
{
    public interface IBankProviderService
    {
        string Get(string key);
        void Update(string key, string url);
    }
}
