﻿using System.Linq;
using Payment.Api.Payment.Models;
using Payment.Api.Payment.Services.Payment;


namespace Payment.Api.Payment.Services.BankProvider
{
    public class BankProviderService : IBankProviderService
    {
        private readonly PaymentContext _context;

        public BankProviderService(PaymentContext context)
        {
            _context = context;
        }

       public string Get(string key)
        {
            return _context.BankProviderItems.FirstOrDefault(p => p.ProviderName == key)?.ProviderUrl;
        }

        public void Update(string key, string url)
        {
            _context.BankProviderItems.Update(new BankProviderData {ProviderName = key, ProviderUrl = url});
            _context.SaveChanges();
        }
    }
}
