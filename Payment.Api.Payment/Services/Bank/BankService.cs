using System.Net.Http;
using System.Text;
using Bank.Api.Bank.Models;
using Newtonsoft.Json;

namespace Payment.Api.Payment.Services.Bank
{
    public class BankService : IBankService
    {
        private readonly HttpClient _httpClient;
        public BankService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public BankResult CallBank(BankData data, string bankUrl)
        {
             var request = new StringContent(JsonConvert.SerializeObject(data),
                 Encoding.UTF8, "application/json");
             var bankResponse =  _httpClient.PostAsync(bankUrl, request).Result;

             BankResult bankResult = new BankResult();
             if (bankResponse.IsSuccessStatusCode)
             {
                 var strResult = bankResponse.Content.ReadAsStringAsync().Result;
                 return  JsonConvert.DeserializeObject<BankResult>(strResult);
             }
            return null;
        }
    }
}