using Bank.Api.Bank.Models;

namespace Payment.Api.Payment.Services.Bank
{
    public interface IBankService
    {
         BankResult CallBank(BankData data, string bankUrl);
    }
}