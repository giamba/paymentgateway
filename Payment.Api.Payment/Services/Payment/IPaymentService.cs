﻿using System.Collections.Generic;
using Payment.Api.Payment.Models;

namespace Payment.Api.Payment.Services.Payment
{
    public interface IPaymentService
    {
        PaymentData Pay(PaymentData data);

        PaymentData GetPayment(long paymentId);

        List<PaymentData> GetAllPayments();
    }
}
