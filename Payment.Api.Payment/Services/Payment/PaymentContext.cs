﻿using Microsoft.EntityFrameworkCore;
using Payment.Api.Payment.Models;

namespace Payment.Api.Payment.Services.Payment
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options)
        {
        }

        public DbSet<PaymentData> PaymentItems { get; set; }
        public DbSet<BankProviderData> BankProviderItems { get; set; }


    }
}
