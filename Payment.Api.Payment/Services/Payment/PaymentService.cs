﻿using System;
using System.Collections.Generic;
using System.Linq;
using Payment.Api.Payment;
using Payment.Api.Payment.Models;
using Payment.Api.Payment.Services;

namespace Payment.Api.Payment.Services.Payment
{
    public class PaymentService : IPaymentService
    {
        private readonly PaymentContext _context;

        public PaymentService(PaymentContext context)
        {
            _context = context;
        }

        public PaymentData Pay(PaymentData data)
        {
            data.Timestamp = DateTime.Now;
            _context.PaymentItems.Add(data);
            _context.SaveChanges();
            return data;
        }
        public PaymentData GetPayment(long paymentId)
        {
            return _context.PaymentItems.FirstOrDefault(p => p.PaymentId == paymentId);
        }

        public List<PaymentData> GetAllPayments()
        {
            return _context.PaymentItems.ToList();
        }
    }
}
