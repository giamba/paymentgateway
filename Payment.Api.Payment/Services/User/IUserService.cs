﻿using Payment.Api.Payment.Models;
namespace Payment.Api.Payment.Services.User
{
    public interface IUserService
    {
        UserData Authenticate(string username, string password);
    }
}
