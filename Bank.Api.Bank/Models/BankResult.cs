﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Bank.Api.Bank.Models
{
    public enum BankStatus
    {
        Ok = 1,
        InsufficientMoney = 2,
        CardNotValid = 3,
        CardExpired = 4,
    }

    public class BankResult
    {
        public long BankId { get; set; }
        public BankStatus Status { get; set; }
    }
}
