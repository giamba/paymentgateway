﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank.Api.Bank.Models
{
    public class BankData
    {
        public long BankId { get; set; }
        public string CardNumber { get; set; }
        public DateTime ExpDate { get; set; }
        public string OwnerName { get; set; }
        public string OwnerSurname { get; set; }
        public DateTime Timestamp { get; set; }
        public decimal Amount { get; set; }
        public string Cvv { get; set; }
    }
}
