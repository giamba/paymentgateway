﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Payment.Api.Models;
using Payment.Api.Models.DTO;
using Payment.Api.Payment.Services.Bank;
using Payment.Api.Payment.Services.BankProvider;
using Payment.Api.Payment.Services.Payment;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using Payment.Api.Payment.Models;

namespace Payment.Api.Controllers
{
    //[Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;
        private readonly IBankProviderService _bankProviderService;
        private readonly IBankService _bankService;
    
        public PaymentController(IPaymentService paymentService, IBankProviderService bankProviderService, IBankService bankService)
        {
            _paymentService = paymentService;
            _bankProviderService = bankProviderService;
            _bankService = bankService;
        }

        /// <summary>
        /// Insert a new payment
        /// </summary>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     POST /Payment/Pay
        ///     {
        ///          "cardNumber": "4993506392650054",
        ///          "expDate": "2019-08-26",
        ///          "cvv": "123",
        ///          "amount": 1
        ///     }
        ///
        /// </remarks>
        /// <param name="paymentIn"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("Pay")]
        public IActionResult Pay([FromBody] PaymentDto paymentIn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //get the bank url from the database
            var bankUrl = _bankProviderService.Get("BankProviderVisa");

            //call the bank
            var bankResult = _bankService.CallBank(PaymentDtoToBank.Map(paymentIn), bankUrl);
            if(bankResult == null)
                return new BadRequestObjectResult(new ErrorMessageDto { Message = "Bank service error", CurrentDate = DateTime.Now });


            //save the result
            var paymentResult = _paymentService.Pay(
                    new PaymentData
                    {
                        BankId = bankResult.BankId,
                        Status = (PaymentStatus)bankResult.Status,
                        CardNumber = paymentIn.CardNumber,
                        Amount = paymentIn.Amount,
                        ExpDate = paymentIn.ExpDate
                    });

            return CreatedAtAction(nameof(GetPaymentDetail),
                                   new { paymentResult.PaymentId },
                                   new PaymentResultDto
                                   {
                                       PaymentId = paymentResult.PaymentId,
                                       Status = (PayStatus)bankResult.Status,
                                       StatusDescription = bankResult.Status.ToString()
                                   });

        }

        /// <summary>
        /// Returns the payment detail
        /// </summary>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     GET /Payment/Pay/PaymentId=1
        ///
        /// </remarks>
        [HttpGet]
        [ActionName("GetPaymentDetail")]
        public IActionResult GetPaymentDetail([Required]long paymentId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var detail = _paymentService.GetPayment(paymentId);
            if (detail != null)
            {
                return new OkObjectResult(
                    PaymentDataToPaymentDetailDto.Map(detail));
            }
            return new NotFoundObjectResult(new ErrorMessageDto { Message = "Payment not found", CurrentDate = DateTime.Now });
        }


        /// <summary>
        /// Returns all the payments
        /// </summary>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     GET /Payment/Pay/GetAllPayments
        ///
        /// </remarks>
        [HttpGet]
        [ActionName("GetAllPayments")]
        public IActionResult GetAllPayments()
        {
            var payments = _paymentService.GetAllPayments();
            if (payments != null)
            {

                return new OkObjectResult(payments.Select(p => PaymentDataToPaymentDetailDto.Map(p)).ToList());
            }
            return new NotFoundObjectResult(new ErrorMessageDto { Message = "Payments not found", CurrentDate = DateTime.Now });
        }

        private HttpResponseMessage CallTheBank(IHttpClientFactory clientFactory, string bankUrl, PaymentDto paymentIn)
        {
            var request = new StringContent(JsonConvert.SerializeObject(PaymentDtoToBank.Map(paymentIn)),
                Encoding.UTF8, "application/json");
            return clientFactory.CreateClient().PostAsync(bankUrl, request).Result;
        }

    }
}
