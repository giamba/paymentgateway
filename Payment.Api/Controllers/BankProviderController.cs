﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Payment.Api.Models.DTO;
using Payment.Api.Payment.Models;
using Payment.Api.Payment.Services.BankProvider;

namespace Payment.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BankProviderController : ControllerBase
    {
        private readonly IBankProviderService _bankProviderService;

        public BankProviderController(IBankProviderService bankProviderService)
        {
            _bankProviderService = bankProviderService;
        }

        /// <summary>
        /// Update Bank Provider url
        /// </summary>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     POST /BankProvider/Update
        ///     {
        ///          "key": "BankProviderVisa",
        ///          "url": "http://localhost:50063/api/Bank/Pay"
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [ActionName("Update")]
        public IActionResult Update([FromBody] BankProviderData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _bankProviderService.Update(data.ProviderName, data.ProviderUrl);

            return Ok();
        }

        /// <summary>
        /// Returns the bank provider url
        /// </summary>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     GET /BankProvider/Get/key=BankProviderVisa
        ///
        /// </remarks>
        [HttpGet]
        [ActionName("Get")]
        public IActionResult Get(string key)
        {
            var detail = _bankProviderService.Get(key);
            if (detail != null)
            {
                return new OkObjectResult(detail);

            }
            return new NotFoundObjectResult(new ErrorMessageDto { Message = "BankProvider not found", CurrentDate = DateTime.Now });
        }
    }
}