﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Payment.Api.Payment.Models;
using Payment.Api.Payment.Services.User;

namespace Payment.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// User this to retrieve the Token (No json input modification required)
        /// </summary>
        [AllowAnonymous]
        [HttpPost]
        [ActionName("Authenticate")]
        public IActionResult Authenticate([FromBody]UserData userParam)
        {
            var user = _userService.Authenticate("test", "test");

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }
    }
}