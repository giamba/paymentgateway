﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Payment.Api.Payment.Services.BankProvider;
using Payment.Api.Payment.Services.Payment;

namespace Payment.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            //2. Find the service layer within our scope.
            using (var scope = host.Services.CreateScope())
            {
                //3. Get the instance of PaymentContext in our services layer
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<PaymentContext>();

                //4. Call the DataGenerator to create sample data
                BankProviderGenerator.Initialize(services);
            }
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        
    }
}
