﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog.Extensions.Logging;
using NLog.Web;
using Payment.Api.Middleware;
using Payment.Api.Payment.Services.Bank;
using Payment.Api.Payment.Services.BankProvider;
using Payment.Api.Payment.Services.Payment;
using Payment.Api.Payment.Services.User;

namespace Payment.Api
{
    public class Startup
    {
        readonly IConfiguration _configuration;
        readonly string _secretKey;
        readonly bool _useAuthentication;
        readonly string _inMemoryDatabaseName;
        readonly bool _useRealBankService;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
            _secretKey = _configuration.GetSection("Secret").Value;
            _useAuthentication = Convert.ToBoolean(_configuration.GetSection("UseAuthentication").Value);
            _inMemoryDatabaseName = _configuration.GetSection("InMemoryDatabaseName").Value;
            _useRealBankService = Convert.ToBoolean(_configuration.GetSection("UseRealBankService").Value);
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<PaymentContext>(opt =>
               opt.UseInMemoryDatabase(_inMemoryDatabaseName));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            ConfigureAuthentication(services, _useAuthentication, _secretKey);

            ConfigureSwaggerGen(services, _useAuthentication);


            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IBankProviderService, BankProviderService>();

            services.AddHttpClient<IBankService, BankService>();
             
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            env.ConfigureNLog("nlog.config");
            loggerFactory.AddNLog();

            app.UseSwagger();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Checkout.com Payment Gateway V1");
                c.RoutePrefix = string.Empty;
            });

            //Logging Middleware
            app.UseMiddleware<LogMiddleware>();
            // Exception Middleware
            app.UseMiddleware<ExceptionMiddleware>();

            app.UseAuthentication();

            app.UseMvc();
        }

        void ConfigureAuthentication(IServiceCollection services, bool authEnabled, string secretKey)
        {
            // configure jwt authentication
            var key = Encoding.ASCII.GetBytes(secretKey);
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                    .AddJwtBearer(x =>
                    {
                        x.RequireHttpsMetadata = false;
                        x.SaveToken = true;
                        x.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(key),
                            ValidateIssuer = false,
                            ValidateAudience = false
                        };
                    });
        }

        void ConfigureSwaggerGen(IServiceCollection services, bool authEnabled)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Checkout.com Payment Gateway", Version = "v1" });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                  c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        Description =
                            "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                        Scheme = "Bearer"

                    });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                },
                                Scheme = "oauth2",
                                Name = "Bearer",
                                In = ParameterLocation.Header,
                            },
                            new List<string>()
                        }
                    });
                
            });
        }

    }
}
