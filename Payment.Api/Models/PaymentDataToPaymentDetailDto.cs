﻿using Payment.Api.Models.DTO;
using Payment.Api.Payment.Models;

namespace Payment.Api.Models
{
    public class PaymentDataToPaymentDetailDto
    {
        public static PaymentDetailDto Map(PaymentData data)
        {
            string cardMask = data.CardNumber.Substring(data.CardNumber.Length - 4).PadLeft(data.CardNumber.Length, '*');

            return new PaymentDetailDto
            {
                PaymentId = data.PaymentId,
                BankId = data.BankId,
                CardNumber = cardMask,
                Amount = data.Amount,
                ExpDate = data.ExpDate,
                Status = data.Status,
                Timestamp = data.Timestamp
            };
        }

    }
}
