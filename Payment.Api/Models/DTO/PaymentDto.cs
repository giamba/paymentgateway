﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Payment.Api.Models.DTO
{
    public class PaymentDto
    {
        //[JsonIgnore]
        public long PaymentId { get; set; }
        [Required]
        [CreditCard]
        public string CardNumber { get; set; }
        [Required]
        public DateTime ExpDate { get; set; }
        [Required]
        public string Cvv { get; set; } 
        [Required]
        [Range(1, 999.99)]
        public decimal Amount { get; set; }
    }
}
