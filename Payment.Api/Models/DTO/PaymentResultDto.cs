﻿namespace Payment.Api.Models.DTO
{
    public enum PayStatus
    {
        Ok = 1,
        InsufficientMoney = 2,
        CardNotValid = 3,
        CardExpired = 4,
    }

    public class PaymentResultDto
    {
        public long PaymentId { get; set; }
        public PayStatus Status { get; set; }
        public string StatusDescription { get; set; }
    }
}
