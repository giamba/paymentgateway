﻿using System;

namespace Payment.Api.Models.DTO
{
    public class ErrorMessageDto
    {
        public string Message { get; set; }

        public DateTime CurrentDate { get; set; }
    }
}
