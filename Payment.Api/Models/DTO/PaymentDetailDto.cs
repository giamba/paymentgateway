﻿using System;
using Payment.Api.Payment.Models;


namespace Payment.Api.Models.DTO
{
    public class PaymentDetailDto
    {
        public long PaymentId { get; set; }
        public long BankId { get; set; }
        public string CardNumber { get; set; }
        public DateTime ExpDate { get; set; }
        public DateTime Timestamp { get; set; }
        public decimal Amount { get; set; }
        public PaymentStatus Status { get; set; }
    }
}
