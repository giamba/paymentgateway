﻿using Bank.Api.Bank.Models;
using Payment.Api.Models.DTO;

namespace Payment.Api.Models
{
    public class PaymentDtoToBank
    {

        public static BankData Map(PaymentDto data)
        {
            return new BankData
            {
                CardNumber = data.CardNumber,
                Amount = data.Amount,
                ExpDate = data.ExpDate,
                Cvv = data.Cvv
            };
        }

    }
}
