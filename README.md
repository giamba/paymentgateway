# Payment Gateway API 
A RESTful Payment Gateway API 

The application use the following in memory tables:  
- PaymentItems: contains all the payments.  
- BankProviderItems: used to store the url of the bank service. The url can be updated at run time. See action /api/BankProvider/Update on test page. 


## Build and Run 
~~~~  
#Download this project 
git clone git@bitbucket.org:giamba/paymentgateway.git

#Build
cd paymentgateway
dotnet build

#Run Payment Api
dotnet run --project Payment.Api/Payment.Api.csproj
#Test Page: http://localhost:5000/index.html

#Run Bank Api (Simulator)
dotnet run --project Bank.Api/Bank.Api.csproj
#Run at: http://localhost:5001
~~~~

## Tests 
~~~~
# Run the Tests
dotnet test Payment.Api.Test/Payment.Api.Test.csproj
~~~~

## Manual Tests 
~~~~
Go to http://localhost:5000/index.html
~~~~

1. Expand the action /api/User/Authenticate
    - Press "Try it out"
    - Press "Execute"
	- Copy the token from Response body
2. Press Authorize (Top Right)
    - Insert in the Value field: Bearer[space]token previously copied"
    - Press "Execute"
	- es:
	 ```
       Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjEiLCJuYmYiOjE1NjcwMjYzMjgsImV4cCI6MTU2NzYzMTEyOCwiaWF0IjoxNTY3MDI2MzI4fQ.2iLYmmHhFc9KUGgDdgGzJRxbqZj3QPL7b3Fu5ak_RQk
	 ```
3. Expand the action /api/Payment/Pay
    - Press "Try it out"
	- Copy the Sample request in the Request body like
	```
	{
     "cardNumber": "4088805351381941",
     "expDate": "2019-08-26",
     "cvv": "123",
     "amount": 1
    }
    ```
    - Press "Execute"
	- You should receive
	```
	{
		"paymentId": 1,
		"status": 1
	}
	```
4. Expand the action /api/Payment/GetPaymentDetail
    - Press "Try it out"
    - Insert paymentId (1)
	- Press "Execute"

## Test Cards 
The Bank.Api simulator returns differents status based on the last digit of the card that you send.
~~~~
4088805351381941 Ok (last digit = 1)

4861225937633322 InsufficientMoney (last digit = 2)

4432781988160623 CardNotValid (last digit = 3)

4010960120517664 CardExpired (last digit = 4)
~~~~

## Switch the application to use the real bank service 
~~~~
Go to the test page http://localhost:5000/index.html and use the BankProvider section.
~~~~

## Application log path 
~~~~
{baseDir}\Payment.Api\bin\Debug\netcoreapp2.2\logs\GDStationaryNetCore\PaymentApi-[year]-[month]-[day].log
~~~~

## Architecture overview 
- The validation has been implemented using the out of the box dotenetcore property attribute validation.
- The documentation and the client has been implemented using Swagger. Swagger is usefuel to smoke test the api and to undestand better requests and responses.
- The application dosn't persist any data, it use EF in memory database.
- The logging has been implemented using NLog. 
- Custom Middleware to log all the requests and the respons
- Custom Middleware to trap all the exceptions   
- Out of the box dotnetcore middlleware for autentication.
- Tests has been mplemented with xUnit and Moq.
- Integration tests/simulation implemented with WebApplicationFactory and xBehave.net
 