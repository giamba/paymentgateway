﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Newtonsoft.Json;
using Payment.Api.Models.DTO;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Bank.Api.Bank.Models;
using Payment.Api.Payment.Models;
using Xbehave;
using Xunit;
using Payment.Api.Payment.Services.Bank;

namespace Payment.Api.Test
{
   public class ApiSimulator : IClassFixture<WebApplicationFactory<Startup>>
   {
       private readonly WebApplicationFactory<Startup> _factory;


       public ApiSimulator(WebApplicationFactory<Startup> factory)
       {
           _factory = factory;
       }

       private async Task<string> GivenAnAuthenticatedUserAsync(HttpClient client) {

           var request =  new StringContent(JsonConvert.SerializeObject(new UserData()),
                                               Encoding.UTF8, "application/json");
           var response = await client.PostAsync("/api/User/Authenticate", request);
           var responseResult = response.Content.ReadAsStringAsync().Result;
           dynamic jsonResult = JsonConvert.DeserializeObject(responseResult);
           return jsonResult.token;  
       }

       private HttpClient GivenClientWithAMockBankServiceWith(BankStatus returnedStatus)
       {
          //given an ok response from the bank
           var mockBankService = new Mock<IBankService>();
           mockBankService.Setup(x => x.CallBank(It.IsAny<BankData>(),It.IsAny<string>())).Returns(new BankResult { Status = returnedStatus, BankId = 123456 });
           var client = _factory.WithWebHostBuilder(builder =>
           {
              builder.ConfigureTestServices(services =>
              {
                  var descriptorToRemove = services.FirstOrDefault(d => d.ServiceType == typeof(IBankService));
                  services.Remove(descriptorToRemove);
                  services.AddSingleton(mockBankService.Object);
              });
           }).CreateClient();
           return client;
       }

       StringContent GivenAValidRequest()
       {
           var json = JsonConvert.SerializeObject(new PaymentDto
           {
               Amount = 1m,
               CardNumber = "4628033338930667",
               Cvv = "123",
               ExpDate = DateTime.Now
           });

           return new StringContent(json, Encoding.UTF8, "application/json");
       }

       PayStatus ReadStatusCode(Task<string> response )
       {
           var str = response.Result;
           dynamic json = JsonConvert.DeserializeObject(str);
           return (PayStatus) Convert.ToInt32(json.status);
       }

       async Task<HttpResponseMessage> SubmitRequestAsync(HttpClient client, string path, string token, StringContent request)
       {
           client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
           return await client.PostAsync(path, request);
       }


       [Scenario]
       public void ResponseOkFromBank(string token, StringContent request, HttpClient client, HttpResponseMessage answer)
       {
           "Given a mocked http client with bank result Ok"
                 .x(() => client = GivenClientWithAMockBankServiceWith(BankStatus.Ok));
           "And an authenticated user with token"
               .x(async () => token = await GivenAnAuthenticatedUserAsync(client));
           "And a valid request"
               .x(() => request = GivenAValidRequest());
           "When submitting a Pay request"
               .x(async () => answer = await SubmitRequestAsync(client, "api/Payment/Pay", token, request));
           "Then the result should be ok"
               .x(() => Assert.Equal(PayStatus.Ok,
                                     ReadStatusCode(answer.Content.ReadAsStringAsync())));
       }

       [Scenario]
       public void ResponseInsufficentMoneyFromBank(HttpClient client, string token, StringContent request,  HttpResponseMessage answer)
       {
           "Given a mocked http client with bank result InsufficientMoney"
                .x(() => client = GivenClientWithAMockBankServiceWith(BankStatus.InsufficientMoney));
           "And an authenticated user with token"
               .x(async () => token = await GivenAnAuthenticatedUserAsync(client));
           "And a valid request"
               .x(() => request = GivenAValidRequest());
           "When submitting a Pay request"
               .x(async () => answer = await SubmitRequestAsync(client, "api/Payment/Pay", token, request));
           "Then the result should be InsufficientMoney"
               .x(() => Assert.Equal(PayStatus.InsufficientMoney,
                                     ReadStatusCode(answer.Content.ReadAsStringAsync())));
       }

       [Scenario]
       public void ResponseCardExpiredFromBank(string token, StringContent request, HttpClient client, HttpResponseMessage answer)
       {
           "Given a mocked http client with bank result CardExpired"
                .x(() => client = GivenClientWithAMockBankServiceWith(BankStatus.CardExpired));
           "And an authenticated user with token"
               .x(async () => token = await GivenAnAuthenticatedUserAsync(client));
           "And a valid request"
               .x(() => request = GivenAValidRequest());
           "When submitting a Pay request"
               .x(async () => answer = await SubmitRequestAsync(client, "api/Payment/Pay", token, request));
           "Then the result should be CardExpired"
               .x(() => Assert.Equal(PayStatus.CardExpired,
                                     ReadStatusCode(answer.Content.ReadAsStringAsync())));
       }
   }
}
