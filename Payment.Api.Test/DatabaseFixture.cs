﻿using Microsoft.EntityFrameworkCore;
using Payment.Api.Payment.Services.Payment;
using System;

namespace Payment.Api.Test
{
    public class DatabaseFixture : IDisposable
    {
        public PaymentContext PaymentDbContext { get; private set; }

        public DatabaseFixture()
        {
            var optBuilder = new DbContextOptionsBuilder<PaymentContext>();
            optBuilder.UseInMemoryDatabase("PaymentDatabase");
            
            PaymentDbContext = new PaymentContext(optBuilder.Options);
        }
        public void Dispose()
        {
            PaymentDbContext.Dispose();
        }
    }
}
