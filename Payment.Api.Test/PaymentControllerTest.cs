﻿using System;
using Bank.Api.Bank.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Payment.Api.Controllers;
using Payment.Api.Models.DTO;
using Payment.Api.Payment;
using Payment.Api.Payment.Models;
using Payment.Api.Payment.Services.Bank;
using Payment.Api.Payment.Services.BankProvider;
using Payment.Api.Payment.Services.Payment;
using Xunit;

namespace Payment.Api.Test
{
    public class PaymentControllerTest : IClassFixture<DatabaseFixture>
    {
        readonly DatabaseFixture _fixture;

        public PaymentControllerTest(DatabaseFixture fixture)
        {
            _fixture = fixture;
        }

        PaymentService GivenAPaymentService()
        {
            return new PaymentService(_fixture.PaymentDbContext);
        }

        PaymentDetailDto GetPaymentDetailById(PaymentController controller, long paymentId)
        {
            var result = controller.GetPaymentDetail(paymentId);
            var paymentResult = Assert.IsType<OkObjectResult>(result);
            return paymentResult.Value as PaymentDetailDto;
        }


        IBankProviderService GivenABankProviderService()
        {
            Mock<IBankProviderService> bankProviderService = new Mock<IBankProviderService>();
            bankProviderService.Setup(x => x.Get(It.IsAny<string>())).Returns("xxx");
            return bankProviderService.Object;
        }


        IBankService GivenAMokedBankService(BankStatus status, long bankId)
        {
            Mock<IBankService> bankService = new Mock<IBankService>();
            bankService.Setup(x => x.CallBank(It.IsAny<BankData>(), It.IsAny<string>())).Returns(new BankResult{Status = status, BankId = bankId});
            return bankService.Object;
        }

       
        [Fact]
        void GivenABankServiceResultInsufficientMoney_When_Pay_Then_InsufficientMoneyResult()
        {
            var paymentToInsert = new PaymentDto
                {CardNumber = "4861225937633322", Amount = 0, Cvv = "123", ExpDate = DateTime.Now};

            IPaymentService paymentService = GivenAPaymentService();
            IBankProviderService bankProviderServiceMock = GivenABankProviderService();
            IBankService bankserviceMock = GivenAMokedBankService(BankStatus.InsufficientMoney, 123);


            //Act
            var controller = new PaymentController(paymentService, bankProviderServiceMock, bankserviceMock);
            var result = controller.Pay(paymentToInsert);

            //Assert
            var createdResult = Assert.IsType<CreatedAtActionResult>(result); //201
            var resultDto = (createdResult.Value as PaymentResultDto);
            //Payment has been inserted
            var createdPayment = GetPaymentDetailById(controller, resultDto.PaymentId);
            Assert.Equal(resultDto.PaymentId, createdPayment.PaymentId);
            Assert.Equal(paymentToInsert.CardNumber.GetLast(4), createdPayment.CardNumber.GetLast(4));
            Assert.Equal(paymentToInsert.Amount, createdPayment.Amount);
            Assert.Equal(PaymentStatus.InsufficientMoney, createdPayment.Status);
        }


        [Fact]
        void GivenABankServiceResultOk_When_Pay_Then_OkResult()
        {
            //Arrange
            var paymentToInsert = new PaymentDto { CardNumber = "4154595012643479", Amount = 10, Cvv = "123", ExpDate = DateTime.Now };
            IPaymentService paymentService = GivenAPaymentService();
            IBankProviderService bankProviderServiceMock = GivenABankProviderService();
            IBankService bankServiceMock = GivenAMokedBankService(BankStatus.Ok, 123);
            
            //Act
            var controller = new PaymentController(paymentService, bankProviderServiceMock, bankServiceMock);
            var result = controller.Pay(paymentToInsert);
            //Assert
            var createdResult = Assert.IsType<CreatedAtActionResult>(result); //201
            var resultDto = (createdResult.Value as PaymentResultDto);

            //Payment has been inserted
            var createdPayment = GetPaymentDetailById(controller, resultDto.PaymentId);
            Assert.Equal(resultDto.PaymentId, createdPayment.PaymentId);
            Assert.Equal(paymentToInsert.CardNumber.GetLast(4), createdPayment.CardNumber.GetLast(4));
            Assert.Equal(paymentToInsert.Amount, createdPayment.Amount);
            Assert.Equal(PaymentStatus.Ok, createdPayment.Status);
        }


        [Fact]
        void GivenAnExistingPayment_When_GetPaymentDetail_Then_ThePaymentIsReturned()
        {
            //Arrange
            var paymentToInsert = new PaymentDto { CardNumber = "4154595012643479", Amount = 10, Cvv = "123", ExpDate = DateTime.Now };
            IPaymentService paymentService = GivenAPaymentService();
            IBankProviderService bankProviderServiceMock = GivenABankProviderService();
            IBankService bankServiceMock = GivenAMokedBankService(BankStatus.Ok, 123);

            //Act
            var controller = new PaymentController(paymentService, bankProviderServiceMock, bankServiceMock);
            var payResult = controller.Pay(paymentToInsert);

            //Assert
            var createdResult = Assert.IsType<CreatedAtActionResult>(payResult); //201
            var createdPayment = createdResult.Value as PaymentResultDto;

            //Act
            var detailResult = controller.GetPaymentDetail(createdPayment.PaymentId);

            //Assert
            var result = Assert.IsType<OkObjectResult>(detailResult); //200
        }

        [Fact]
        void GivenTwoExistingPayments_When_GetPaymentDetail_Then_ThePaymentsAreReturned()
        {
            //Arrange
            IPaymentService paymentService = GivenAPaymentService();
            IBankProviderService bankProviderServiceMock = GivenABankProviderService();
            IBankService bankserviceMock = GivenAMokedBankService(BankStatus.Ok, 123);

            var paymentToInsert1 = new PaymentDto { CardNumber = "4154595012643479", Amount = 10, Cvv = "123", ExpDate = DateTime.Now };
            var paymentToInsert2 = new PaymentDto { CardNumber = "4154595012643479", Amount = 10, Cvv = "123", ExpDate = DateTime.Now };

            //Act
            var controller = new PaymentController(paymentService, bankProviderServiceMock, bankserviceMock);
            var result1 = controller.Pay(paymentToInsert1);
            var result2 = controller.Pay(paymentToInsert2);

            //Assert
            var createdResult1 = Assert.IsType<CreatedAtActionResult>(result1); //201
            var paymentId1 = (createdResult1.Value as PaymentResultDto).PaymentId;

            var createdResult2 = Assert.IsType<CreatedAtActionResult>(result2); //201
            var paymentId2 = (createdResult2.Value as PaymentResultDto).PaymentId;

            var createdPayment1 = GetPaymentDetailById(controller, paymentId1);
            var createdPayment2 = GetPaymentDetailById(controller, paymentId2);

            Assert.Equal(paymentToInsert1.CardNumber.GetLast(4), createdPayment1.CardNumber.GetLast(4));
            Assert.Equal(paymentToInsert2.CardNumber.GetLast(4), createdPayment2.CardNumber.GetLast(4));
        }

        [Fact]
        void GivenANonExistingPayment_When_GetPaymentDetail_Then_ThePaymentIsReturned()
        {
            //Arrange
            IPaymentService paymentService = GivenAPaymentService();
            IBankProviderService bankProviderServiceMock = GivenABankProviderService();
            IBankService bankserviceMock = GivenAMokedBankService(BankStatus.Ok, 123);

            //Act
            var controller = new PaymentController(paymentService, bankProviderServiceMock, bankserviceMock);
            var result = controller.GetPaymentDetail(123456);

            //Assert
            var createdResult = Assert.IsType<NotFoundObjectResult>(result); //404
        }
    }

    public static class StringExtension
    {
        public static string GetLast(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }
    }
}
